<div align="center">
    <img src="./swift-gpio.png" width="200" max-width="90%" alt="Blanket"/>
    <h1 align="center">Swift GPIO</h1>
    <a href="https://gitlab.com/thecb4/swift-gpio/-/commits/main">
      <img src="https://gitlab.com/thecb4/swift-gpio/badges/main/pipeline.svg" />
    </a>
    <img src="https://img.shields.io/badge/Swift-5.6-orange.svg" />
    <img src="https://img.shields.io/badge/platform-linux-blue" />
    <a href="https://twitter.com/_thecb4">
      <img src="https://img.shields.io/badge/twitter-@_thecb4-blue.svg?style=flat" />
    </a>
</div>

<hr/>


Swift library for interacting with GPIO on dev boards like Raspberry Pi


## Building the Image

## Installation

## Usage

### build


### unit test


### ui testing


## Changelog
Find changes [here](./CHANGELOG.md)

## License
[MIT](./LICENSE.md)

## Contributors
[The Coding Elves](./CONTRIBUTING.md)
