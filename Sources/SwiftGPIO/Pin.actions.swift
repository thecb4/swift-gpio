extension Pin {
  public func provision(for direction: Direction) throws {
    try manager.provision(self, for: direction)
  }

  public func release() throws {
    try manager.release(self)
  }

  public func set(state: State) throws {
    // guard self.direction != .none else { throw GPIOError.directionWriteError(self) }
    try manager.set(state: state, for: self)
  }
}
