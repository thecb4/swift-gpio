extension Platform {
  public enum GPIOName: String, ExpressibleByStringLiteral {

    // https://forums.swift.org/t/enums-and-dynamic-member-lookup/18001/2
    public typealias StringLiteralType = String

    public init(stringLiteral value: StringLiteralType) {
      self = GPIOName.resolve(value)
    }

    case p00, p01, p02, p03, p04, p05, p06, p07, p08, p09
    case p10, p11, p12, p13, p14, p15, p16, p17, p18, p19
    case p20, p21, p22, p23, p24, p25, p26, p27, p28, p29
    case p30, p31, p32, p33, p34, p35, p36, p37, p38, p39
    case p40, p41, p42, p43, p44, p45, p46, p47, p48, p49
    case none

    static func resolve(_ string: StringLiteralType) -> GPIOName {
      if let name = GPIOName(rawValue: string) {
        return name
      } else {
        return .none
      }
    }

  }
}

public typealias GPIOName = Platform.GPIOName
extension GPIOName: Codable, CaseIterable {}
