public struct RaspberryPiRev1: Board {

  public init() {}

  public let platform: Platform = .RaspberryPiRev1
  public let baseAddress: GPIOAddress = 0x2000_0000
  public let manager: BoardManager = GeneralBoardManager()

  public let pinMap: [GPIOName: PinID] = [
    .p00: 0,
    .p01: 1,
    // .p02: 2,
    // .p03: 3,
    .p04: 4,
    // .p05: 5,
    // .p06: 6,
    .p07: 7,
    .p08: 8,
    .p09: 9,

    .p10: 10,
    .p11: 11,
    // .p12: 12,
    // .p13: 13,
    .p14: 14,
    .p15: 15,
    // .p16: 16,
    .p17: 17,
    .p18: 18,
    // .p19: 19,

    // .p20: 20,
    .p21: 21,
    .p22: 22,
    .p23: 23,
    .p24: 24,
    .p25: 25,
      // .p26: 26,
      // .p27: 27,
      // .p28: 28,
      // .p29: 29,

      // .p30: 30,
      // .p31: 31,
      // .p32: 32,
      // .p33: 33,
      // .p34: 34,
      // .p35: 35,
      // .p36: 36,
      // .p37: 37,
      // .p38: 38,
      // .p39: 39,

      // .p40: 40,
      // .p41: 41,
      // .p42: 42,
      // .p43: 43,
      // .p44: 44,
      // .p45: 45,
      // .p46: 46,
      // .p47: 47,
      // .p48: 48,
      // .p49: 49
  ]

}
