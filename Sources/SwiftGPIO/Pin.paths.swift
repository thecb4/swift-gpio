import SystemPackage

extension Pin {
  static let basePath: FilePath = "/sys/class/gpio"

  static var exportPath: FilePath {
    var path = basePath
    path.components.append("export")
    return path
  }

  static var unexportPath: FilePath {
    var path = basePath
    path.components.append("unexport")
    return path
  }

  public var pinPath: FilePath {
    get throws {
      var path = Pin.basePath
      guard let component = FilePath.Component("gpio\(id)") else {
        throw GPIOError.pathError("\(Pin.basePath)\\gpio\(id)")
      }
      path.components.append(component)
      return path
    }
  }

  public var directionPath: FilePath {
    get throws {
      guard var path = try? pinPath else { throw GPIOError.pathError("\(Pin.basePath)\\gpio\(id)") }
      path.components.append("direction")
      return path
    }
  }

  public var statePath: FilePath {
    get throws {
      guard var path = try? pinPath else { throw GPIOError.pathError("\(Pin.basePath)\\gpio\(id)") }
      path.components.append("value")
      return path
    }
  }

}
