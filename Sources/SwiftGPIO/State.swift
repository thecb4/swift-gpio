extension Platform.GPIOName {
  public enum State: Int, CustomStringConvertible {
    case high = 1
    case low = 0
    case none = -1

    public var description: String {
      switch self {
        case .high:
          return "high"
        case .low:
          return "low"
        case .none:
          return "none"
      }
    }

    public init?(string: String) {
      guard let int = Int(string) else { return nil }
      self.init(rawValue: int)
    }
  }
}

public typealias State = Platform.GPIOName.State
extension State: Codable, CaseIterable {}
