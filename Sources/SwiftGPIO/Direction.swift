extension Platform.GPIOName {
  public enum Direction: String {
    case `in`, out, none
  }
}

public typealias Direction = Platform.GPIOName.Direction
extension Direction: Codable, CaseIterable {}
