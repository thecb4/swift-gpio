extension Platform.GPIOName {
  public enum Transition: String {
    case none, rising, falling, both
  }
}

public typealias Transition = Platform.GPIOName.Transition
extension Transition: Codable, CaseIterable {}
