import Logging
import SystemPackage

typealias AccessMode = FileDescriptor.AccessMode
typealias OpenOptions = FileDescriptor.OpenOptions

public protocol BoardManager {
  var logger: Logger { get }
  func provision(_ pin: Pin, for direction: Direction) throws
  func release(_ pin: Pin) throws
  func set(direction: Direction, for pin: Pin) throws
  func direction(for pin: Pin) throws -> Direction
  func set(state: State, for pin: Pin) throws
  func state(for pin: Pin) throws -> State
}

struct GeneralBoardManager: BoardManager {
  let logger = Logger(label: "GPIO")
}

extension BoardManager {

  func descriptor(for path: FilePath, mode: AccessMode, options: OpenOptions) throws
    -> FileDescriptor
  {
    try FileDescriptor.open(
      path,
      .writeOnly,
      options: options,
      permissions: .ownerReadWrite
    )
  }

  func export(_ pin: Pin) throws {
    let message = "\(pin.id)"
    let path = Pin.exportPath
    logger.info("export path for \(pin) is \(path)")
    let descriptor = try descriptor(for: path, mode: .writeOnly, options: [.create])
    try descriptor.closeAfter {
      _ = try descriptor.writeAll(message.utf8)
    }
  }

  func set(direction: Direction, for pin: Pin) throws {
    let message = "\(direction)"
    let path = try pin.directionPath
    logger.info("direction path for \(pin) is \(path) with direction \(direction)")
    let descriptor = try descriptor(for: path, mode: .writeOnly, options: [.create])
    try descriptor.closeAfter {
      _ = try descriptor.writeAll(message.utf8)
    }
  }

  func provision(_ pin: Pin, for direction: Direction) throws {
    try export(pin)
    try set(direction: direction, for: pin)
  }

  func direction(for pin: Pin) throws -> Direction {
    let path = try pin.directionPath
    let value = try path.readUTF8String()
    guard let direction = Direction(rawValue: value) else {
      throw GPIOError.directionReadError(pin)
    }
    return direction
  }

  func set(state: State, for pin: Pin) throws {
    let message = "\(state.rawValue)"
    let path = try pin.statePath
    logger.info("state path for \(pin) is \(path) with state \(state)")
    let descriptor = try descriptor(for: path, mode: .writeOnly, options: [.create])
    try descriptor.closeAfter {
      _ = try descriptor.writeAll(message.utf8)
    }
  }

  func state(for pin: Pin) throws -> State {
    let path = try pin.statePath
    let value = try path.readUTF8String()
    guard let state = State(string: value) else { throw GPIOError.stateReadError(pin) }
    return state
  }

  func release(_ pin: Pin) throws {
    let message = "\(pin.id)"
    let path = Pin.unexportPath
    let descriptor = try descriptor(for: path, mode: .writeOnly, options: [.create])
    try descriptor.closeAfter {
      _ = try descriptor.writeAll(message.utf8)
    }
  }
}
