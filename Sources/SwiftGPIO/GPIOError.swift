enum GPIOError: Swift.Error {
  case pathError(String)
  case directionWriteError(Pin)
  case directionReadError(Pin)
  case stateWriteError(Pin)
  case stateReadError(Pin)
}
