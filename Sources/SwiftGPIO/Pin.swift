// MARK: Pins

public typealias PinID = Int

public struct Pin: CustomStringConvertible {
  var platform: Platform
  var baseAddress: GPIOAddress
  var name: GPIOName
  var id: PinID

  var manager: BoardManager

  var direction: Direction = .none
  var transition: Transition = .none
  var state: State = .none

  public var description: String {
    "\(platform):\(name):\(direction):\(transition) = \(state)"
  }

}
