public typealias GPIOAddress = Int

@dynamicMemberLookup
public protocol Board: CustomStringConvertible {
  var platform: Platform { get }
  var baseAddress: GPIOAddress { get }
  var manager: BoardManager { get }
  var pinMap: [GPIOName: PinID] { get }
  subscript(dynamicMember member: GPIOName) -> Pin? { get }

}

extension Board {
  public var description: String {
    "\(platform):\(baseAddress, radix: .hex, prefix: true, toWidth: 8)"
  }

  public subscript(dynamicMember member: GPIOName) -> Pin? {
    guard let id = pinMap[member] else { return nil }
    return Pin(platform: platform, baseAddress: baseAddress, name: member, id: id, manager: manager)
  }

}
