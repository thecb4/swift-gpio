import XCTest

@testable import SwiftGPIO

final class SwiftGPIOTests: XCTestCase {
  func testGPIOFilePath() throws {
    XCTAssertEqual(Pin.basePath, "/sys/class/gpio/")
  }

  func testBoardDescription() throws {
    // given
    let sut = RaspberryPiRev1()
    let expectedDescription = "RaspberryPiRev1:0x20000000"

    //when
    let actualDescription = sut.description

    // then
    XCTAssertEqual(actualDescription, expectedDescription)
  }

  func testPinDescription() throws {
    // given
    let sut = Pin(
      platform: .RaspberryPiRev1, baseAddress: 0, name: .p00, id: 0, manager: GeneralBoardManager())
    let expectedDescription = "RaspberryPiRev1:p00:none:none = none"

    // when
    let actualDescription = "\(sut)"

    // then
    XCTAssertEqual(actualDescription, expectedDescription)
  }

  func testGetPinFromBoardReturnsNil() throws {
    // given
    let sut = RaspberryPiRev1()

    // when
    let pin = sut.p99

    // then
    XCTAssertNil(pin)
  }

  func testGetPin00FromBoardReturnsPin() throws {
    // given
    let sut = RaspberryPiRev1()
    let expectedDescription = "RaspberryPiRev1:p00:none:none = none"

    // when
    guard let pin = sut.p00 else {
      XCTFail()
      return
    }
    let actualDescription = "\(pin)"

    // then
    XCTAssertEqual(actualDescription, expectedDescription)
  }

  func testPinBasePath() throws {
    // given
    let expectedBasePath = "/sys/class/gpio"

    // when
    let actualBasePath = "\(Pin.basePath)"

    // then
    XCTAssertEqual(actualBasePath, expectedBasePath)
  }

  func testPinExportPath() throws {
    // given
    let expectedBasePath = "/sys/class/gpio/export"

    // when
    let actualBasePath = "\(Pin.exportPath)"

    // then
    XCTAssertEqual(actualBasePath, expectedBasePath)
  }

  func testPinPath() throws {
    // given
    let sut = RaspberryPiRev1()
    let expectedDescription = "/sys/class/gpio/gpio0"

    // when
    guard let pin = sut.p00, let path = try? pin.pinPath else {
      XCTFail()
      return
    }

    let actualDescription = "\(path)"

    // then
    XCTAssertEqual(actualDescription, expectedDescription)
  }

  func testPinDirectionPath() throws {
    // given
    let sut = RaspberryPiRev1()
    let expectedDescription = "/sys/class/gpio/gpio0/direction"

    // when
    guard let pin = sut.p00, let path = try? pin.directionPath else {
      XCTFail()
      return
    }

    let actualDescription = "\(path)"

    // then
    XCTAssertEqual(actualDescription, expectedDescription)
  }

  func testPinStatePath() throws {
    // given
    let sut = RaspberryPiRev1()
    let expectedDescription = "/sys/class/gpio/gpio0/value"

    // when
    guard let pin = sut.p00, let path = try? pin.statePath else {
      XCTFail()
      return
    }

    let actualDescription = "\(path)"

    // then
    XCTAssertEqual(actualDescription, expectedDescription)
  }

}
